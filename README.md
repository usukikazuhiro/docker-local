# Environment
## 構成
directory  
  |- docker  
      |- html  
          |- public  
              |- index.html  


- directory配下にdocker-local を設置する
- htmlをLaravelのプロジェクトを差し替えるとLaravelが動かせる

## 使い方
mkdir directory
cd mcg_user
- ディレクトリ作成して移動

git clone git@bitbucket.org:usukikazuhiro/docker-local.git
- dockerをgit cloneして、dockerにリネーム

ln -s ../laravel html
- laravelを使いたいときは、htmlを削除してlaravelのシンボリックリンクに変換する

##  起動
docker-comopose build --no-catch
docker-comopose up -d
## 確認
http://localhost/  
